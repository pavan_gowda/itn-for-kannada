## Code to generate time randomly in Kannada

import random

Hours_num = [[""], ["ಒಂದು","ಒನ್"], ["ಎರಡು","ಟೂ"], ["ಮೂರು","ತ್ರೀ"], ["ನಾಲ್ಕು","ಫೋರ್"], ["ಐದು","ಫೈವ್"], ["ಆರು","ಸಿಕ್ಸ್"], ["ಏಳು","ಸೆವೆನ್"], ["ಎಂಟು","ಏಟ್"], ["ಒಂಬತ್ತು","ನೈನ್"], ["ಹತ್ತು","ಟೆನ್"], ["ಹನ್ನೊಂದು","ಲೆವೆನ್"], ["ಹನ್ನೆರಡು","ಟವೆಲ್"]]

Mins_num = [[""],
    ["ಒಂದು"], ["ಎರಡು"], ["ಮೂರು"], ["ನಾಲ್ಕು"], ["ಐದು","ಫೈವ್"], ["ಆರು"], ["ಏಳು"], ["ಎಂಟು"], ["ಒಂಬತ್ತು"], ["ಹತ್ತು","ಟೆನ್"], ["ಹನ್ನೊಂದು"], ["ಹನ್ನೆರಡು"], ["ಹದಿಮೂರು"], ["ಹದಿನಾಲ್ಕು"], ["ಹದಿನೈದು","ಕಾಲು","ಫಿಫ್ಟೀನ್"], ["ಹದಿನಾರು"], ["ಹದಿನೇಳು"],
    ["ಹದಿನೆಂಟು"], ["ಹತ್ತೊಂಬತ್ತು"], ["ಇಪ್ಪತ್ತು","ಟ್ವೆಂಟಿ"], ["ಇಪ್ಪತ್ತೊಂದು"], ["ಇಪ್ಪತ್ತೆರಡು"], ["ಇಪ್ಪತ್ಮೂರು"], ["ಇಪ್ಪತ್ನಾಲ್ಕು", "ಇಪ್ಪತ್ನಾಕು"], ["ಇಪ್ಪತ್ತೈದು"], ["ಇಪ್ಪತ್ತಾರು"], ["ಇಪ್ಪತ್ತೇಳು"], ["ಇಪ್ಪತ್ತೆಂಟು"], ["ಇಪ್ಪತ್ತೊಂಬತ್ತು"], ["ಮೂವತ್ತು","ವರೆ","ಥರ್ಟಿ"], ["ಮೂವತ್ತೊಂದು"],
    ["ಮೂವತ್ತೆರಡು"], ["ಮೂವತ್ತಮೂರು"], ["ಮೂವತ್ತನಾಲ್ಕು"], ["ಮೂವತ್ತೈದು"], ["ಮೂವತ್ತಾರು"], ["ಮೂವತ್ತೇಳು"], ["ಮೂವತ್ತೆಂಟು"], ["ಮೂವತ್ತೊಂಬತ್ತು"], ["ನಲವತ್ತು","ಫಾರ್ಟಿ"], ["ನಲವತ್ತೊಂದು"], ["ನಲವತ್ತೆರಡು"], ["ನಲವತ್ಮೂರು"], ["ನಲವತ್ತನಾಲ್ಕು"],
    ["ನಲವತ್ತೈದು","ಮುಕ್ಕಾಲು","ಮುಕಾಲು","ಫಾರ್ಟೀಫೈವ್"], ["ನಲವತ್ತಾರು"], ["ನಲವತ್ತೇಳು"], ["ನಲವತ್ತೆಂಟು"], ["ನಲವತ್ತೊಂಬತ್ತು"], ["ಐವತ್ತು"], ["ಐವತ್ತೊಂದು"], ["ಐವತ್ತೆರಡು"], ["ಐವತ್ತಮೂರು"], ["ಐವತ್ನಾಲ್ಕು"], ["ಐವತ್ತೈದು"], ["ಐವತ್ತಾರು"], ["ಐವತ್ತೇಳು"], ["ಐವತ್ತೆಂಟು"],
    ["ಐವತ್ತೊಂಬತ್ತು"], ["ಅರವತ್ತು"]]


def Hours(i):
    num = i
    if i > 12:
        pass
    else:
        s = random.choice(Hours_num[i]) + "," + str(num) + ",digit \t"
        # print(s)
        return s

print(Hours(6))



def Hours1():
    num = random.randint(1,100)
    if num <= 80:
        s = "ಗಂಟೆ," + ":," + "time \t"
    else:
        s = "ಅವರ್," + ":," + "time \t"
    return s

print(Hours1())

def Mins(j):
    num = j
    num1 = random.randint(1,400)
    if j >= 60:
        pass
    else:
        if j==15 and num1<=100:
            s = Mins_num[15][1] + "," + str(num) + ",digit \t"
            print(s)
        elif j==30 and 100<num1<=200:
            s = Mins_num[30][1] + "," + str(num) + ",digit \t"
            print(s)
        elif j==45 and 200<num1<=250:
            s = Mins_num[45][1] + "," + str(num) + ",digit \t"
            print(s)
        elif j==45 and 250<num1<=300:
            s = Mins_num[45][2] + "," + str(num) + ",digit \t"
            print(s)
        else:
            s = random.choice(Mins_num[j]) + "," + str(num) + ",digit \t"
            # print(s)
        return s

print(Mins(15))


def Mins1():
    num2 = random.randint(1, 100)
    if num2 <= 50:
        s = "ನಿಮಿಷ," + "," + "time \t"
    else:
        s = "ಮಿನಿಟ್ಸ್," + "," + "time \t"
    return s

print(Mins1())


def final_time(i,j,count):
    s = Hours(i)
    s1 = Hours1()
    s2 = Mins(j)
    s3 = Mins1()
    num2 = random.randint(1,300)
    NonType = type(None)
    if isinstance(s, NonType) or isinstance(s2, NonType):
        print("Please give us proper date and month!")
    else:
        if num2 <= 100:
            s4 = str(count) + "-" + s + s1 + "\n"
            f = open("Kannada_Time_Data.txt", "a", encoding="UTF-8")
            f.write(s4)
            f.close()
        elif 100 < num2 <= 200:
            s4 = str(count) + "-" + s + s2 + "\n"
            f = open("Kannada_Time_Data.txt", "a", encoding="UTF-8")
            f.write(s4)
            f.close()
        else:
            s4 = str(count) + "-" + s + s1 + s2 + s3 + "\n"
            f = open("Kannada_Time_Data.txt", "a", encoding="UTF-8")
            f.write(s4)
            f.close()
        return s4

#print(final_time(11, 30, 10))


count = 1
li = list(range(1, 13))
li1 = list(range(1, 60))
for i in range(1000):
    new = random.choice(li)
    new1 = random.choice(li1)
    print(new)
    print(new1)
    time_sen = final_time(new, new1, count)
    print(time_sen)
    time_sen_sp =time_sen.split('-')
    print(time_sen_sp)
    time_sen_sp1 = time_sen_sp[1].split()
    hour_sen = time_sen_sp1[0].split(',')
    print(hour_sen)
    hour = hour_sen[1]
    print(len(str(hour)))
    print(hour)
    if len(str(hour)) == 1:
        hour = "0"+str(hour)
    print(hour)
    leng = len(time_sen_sp1)
    time_sen_sp2 = time_sen_sp1[1].split(',')
    time_sen_sp22 = time_sen_sp2[1]
    print(time_sen_sp22)
    if leng > 2:
        time_sen_sp3 = time_sen_sp1[2].split(',')
        time_sen_sp33 = time_sen_sp3[1]
        print(time_sen_sp33)
    min = 0
    print(min)
    if time_sen_sp22.isdigit():
        min = time_sen_sp22
        print(min)
    elif leng > 3 and time_sen_sp33.isdigit():
        min = time_sen_sp33
        print(min)
    print(len(str(min)))
    if len(str(min)) == 1:
        min = "0"+str(min)
    print(min)
    count = count + 1
    print("Time: " + hour + ":" + min + "" + "")
    final = "\n" + time_sen + "\t\t"+ hour + ":" + min + ""
    print("\n" + time_sen + "\t\t"+ hour + ":" + min + "")
    print(final)
    f = open("Kannada_Time_in_Digits.txt", "a", encoding="UTF-8")
    f.write(final)
    f.close()


