import random

# ones_digit = ["zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve",
#               "thirteen", "fourteen",
#               "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"]
# tens_digit = ["ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"]
# hundred = "hundred"
# thousand = "thousand"
# lakh = "lakhs"
# crore = "crores"
ones_digit =[[""],
    ["ಒನ್"], ["ಟೂ"], ["ಥ್ರೀ"], ["ಫೋರ್"], ["ಫೈವ್"], ["ಸಿಕ್ಸ್"], ["ಸೆವೆನ್"], ["ಏಟ್"], ["ನೈನ್"], ["ಟೆನ್"], ["ಲೆವೆನ್"], ["ಟವೆಲ್"], ["ಥರ್ಟೀನ್"], ["ಫೋರ್ಟೀನ್"], ["ಫಿಫ್ಟೀನ್"], ["ಸಿಕ್ಸ್ಟೀನ್"], ["ಸೆವೆಂಟಿನ್"],
    ["ಏಟೀನ್"], ["ನೈನ್ಟೀನ್"], ["ಟ್ವೆಂಟಿ"], ["ಟ್ವೆಂಟಿಒನ್"], ["ಟ್ವೆಂಟಿಟು"], ["ಟ್ವೆಂಟಿಥ್ರೀ"], ["ಟ್ವೆಂಟಿಫೋರ್"], ["ಟ್ವೆಂಟಿಫೈವ್"], ["ಟ್ವೆಂಟಿಸಿಕ್ಸ್"], ["ಟ್ವೆಂಟಿಸೆವೆನ್"], ["ಟ್ವೆಂಟಿಏಟ್"], ["ಟ್ವೆಂಟಿನೈನ್"], ["ಥರ್ಟಿ"], ["ಥರ್ಟಿಒನ್"],
    ["ಥರ್ಟಿಟೂ  "], ["ಥರ್ಟಿಥ್ರೀ"], ["ಥರ್ಟಿಫೋರ್"], ["ಥರ್ಟಿಫೈವ್"], ["ಥರ್ಟಿಸಿಕ್ಸ್"], ["ಥರ್ಟಿಸೆವೆನ್"], ["ಥರ್ಟಿಹಿಟ್"], ["ಥರ್ಟಿನೈನ್"], ["ಪಾರ್ಟಿ"], ["ಫಾರ್ಟಿಒನ್"], ["ಫಾರ್ಟಿಟು"], ["ಫಾರ್ಟಿಥ್ರೀ"], ["ಫಾರ್ಟಿಫೋರ್"],
    ["ಫಾರ್ಟಿಫೈವ್"], ["ಫಾರ್ಟಿಸಿಕ್ಸ್"], ["ಫಾರ್ಟಿಸೆವೆನ್"], ["ಫಾರ್ಟಿಏಟ್"], ["ಫಾರ್ಟಿನೈನ್"], ["ಫಿಫ್ಟಿ"], ["ಫಿಫ್ಟಿಒನ್"], ["ಫಿಫ್ಟಿಟು"], ["ಫಿಫ್ಟಿಥ್ರೀ"], ["ಫಿಫ್ಟಿಫೋರ್"], ["ಫಿಫ್ಟಿಫೈವ್"], ["ಫಿಫ್ಟಿಸಿಕ್ಸ್"], ["ಫಿಫ್ಟಿಸೆವೆನ್"], ["ಫಿಫ್ಟಿಏಟ್"],
    ["ಫಿಫ್ಟಿನೈನ್"], ["ಸಿಕ್ಸ್ಟಿ"], ["ಸಿಕ್ಸ್ಟಿಒನ್"], ["ಸಿಕ್ಸ್ಟಿಟು"], ["ಸಿಕ್ಸ್ಟಿಥ್ರೀ"], ["ಸಿಕ್ಸ್ಟಿಫೋರ್"], ["ಸಿಕ್ಸ್ಟಿಫೈವ್"], ["ಸಿಕ್ಸ್ಟಿಸಿಕ್ಸ್"], ["ಸಿಕ್ಸ್ಟಿಸೆವೆನ್"], ["ಸಿಕ್ಸ್ಟಿಏಟ್"], ["ಸಿಕ್ಸ್ಟಿನೈನ್"], ["ಸೆವೆಂಟಿ"], ["ಸೆವೆಂಟಿಒನ್"], ["ಸೆವೆಂಟೀಟೂ"],
    ["ಸೆವೆಂಟಿಥ್ರೀ"], ["ಸೆವೆಂಟಿಫೋರ್"], ["ಸೆವೆಂಟಿಫೈವ್"], ["ಸೆವೆಂಟಿಸಿಕ್ಸ್"], ["ಸೆವೆಂಟಿಸೆವೆನ್"], ["ಸೆವೆಂಟಿಏಟ್"], ["ಸೆವೆಂಟಿನೈನ್"], ["ಐಟಿ"], ["ಏಟಿಒನ್"], ["ಏಟಿಟೂ"], ["ಏಟಿಥ್ರೀ"], ["ಏಟಿಫೋರ್"], ["ಏಟಿಫೈವ್"], ["ಏಟಿಸಿಕ್ಸ್"],
    ["ಏಟಿಸೆವೆನ್"], ["ಏಟಿಏಟ್"], ["ಏಟಿನೈನ್"], ["ನೈಂಟಿ"], ["ನೈಂಟಿಒನ್"], ["ನೈಂಟಿಟೂ"], ["ನೈಂಟಿಥ್ರೀ"], ["ನೈಂಟಿಫೋರ್"], ["ನೈಂಟಿಫೈವ್"], ["ನೈಂಟಿಸಿಕ್ಸ್"], ["ನೈಂಟಿಸೆವೆನ್"], ["ನೈಂಟಿಏಟ್"], ["ನೈಂಟಿನೈನ್"]]

one_digit =[[""],["ಒನ್"], ["ಟೂ"], ["ಥ್ರೀ"], ["ಫೋರ್"], ["ಫೈವ್"], ["ಸಿಕ್ಸ್"], ["ಸೆವೆನ್"], ["ಏಟ್"], ["ನೈನ್"]]

tens_digit = [[""], ["ಟೆನ್"], ["ಟ್ವೆಂಟಿ"], ["ಥರ್ಟಿ"], ["ಪಾರ್ಟಿ"], ["ಫಿಫ್ಟಿ"], ["ಸಿಕ್ಸ್ಟಿ"], ["ಸೆವೆಂಟಿ"], ["ಐಟಿ"], ["ನೈಂಟಿ"]]
hundred = "ಹಂಡ್ರೆಡ್"
thousand = ["ಥೌಸಂಡ್"]
lakh = ["ಲಾಖ್"]
crore = ["ಕ್ರೋರ್"]



def single_digit(i):
    print(str(i))
    s = random.choice(ones_digit[i]) + "," + str(i) + ",digit\t"
    return s

print(single_digit(20))


def double_digit(i):
    if (i < 20):
        s = single_digit(i)
    elif (i % 10 == 0):
        s = random.choice(tens_digit[int(i / 10)]) + "," + str(int(i)) + ",mag_pop0\t"
    elif (i < 100):
        s = random.choice(tens_digit[int(i / 10)]) + "," + str(int(i / 10)) + ",mag_pop1\t" + single_digit(i % 10)  # ones_digit[i%10-1] +","+str(i%10)+",digit"
    return s

print(double_digit(16))

def triple_digit(i):
    if (i % 100 == 0):
        s = random.choice(one_digit[int(i / 100)]) + "," + str(int(i / 100)) + "00" +",mag_pop0\t" + str(hundred)+",,number"
    else:
        if((i % 100) < 10):
            s = random.choice(one_digit[int(i / 100)]) + "," + str(int(i / 100)) + "0,mag_pop1\t" + str(hundred)+ ",,number\t" + "" + single_digit(i % 100)  # +""
        else:
            s = random.choice(one_digit[int(i / 100)]) + "," + str(int(i / 100)) + ",mag_pop2\t" + str(hundred)+ ",,number\t" + "" + double_digit(i % 100)  # +""
    return s

print(triple_digit(351))


def four_digit(i):
    if (i % 1000 == 0):
        s = single_digit(int(i / 1000)) + random.choice(thousand) + ",000,mag_pop0\t"
    else:
        if (i % 1000 < 10):
            s = single_digit(int(i / 1000)) + random.choice(thousand) + ",00,mag_pop1\t" + single_digit(i % 1000)
        elif (i % 1000 < 100):
            s = single_digit(int(i / 1000)) + random.choice(thousand) + ",0,mag_pop2\t" + double_digit(i % 1000)
        else:
            s = single_digit(int(i / 1000)) + random.choice(thousand) + ",,mag_pop3\t" + triple_digit(i % 1000)
    return s


print(four_digit(9458))

def five_digit(i):
    if (i % 10000 == 0):
        s = double_digit(int(i / 1000)) + random.choice(thousand) + ",000,mag_pop0\t"
    elif (i % 10000 < 10):
        s = double_digit(int(i / 1000)) + random.choice(thousand) + ",00,mag_pop1\t" + single_digit(i % 10000)
    elif (i % 10000 < 100):
        s = double_digit(int(i / 1000)) + random.choice(thousand) + ",0,mag_pop2\t" + double_digit(i % 10000)
    elif (i % 10000 < 1000):
        s = double_digit(int(i / 1000)) + random.choice(thousand) + ",,mag_pop3\t" + triple_digit(i % 10000)
    else:
        s = double_digit(int(i / 1000)) + random.choice(thousand) + ",,mag_pop3\t" + triple_digit(i % 1000)
    return s

print(five_digit(46385))


def six_digit(i):
    if (i % 100000 == 0):
        s = single_digit(int(i / 100000)) + random.choice(lakh) + ",00000,mag_pop0\t"
    elif (i % 100000 < 10):
        s = single_digit(int(i / 100000)) + random.choice(lakh) + ",0000,mag_pop1\t" + single_digit(i % 100000)
    elif (i % 100000 < 100):
        s = single_digit(int(i / 100000)) + random.choice(lakh) + ",000,mag_pop2\t" + double_digit(i % 100000)
    elif (i % 100000 < 1000):
        s = single_digit(int(i / 100000)) + random.choice(lakh) + ",00,mag_pop3\t" + triple_digit(i % 100000)
    elif (i % 100000 < 10000):
        s = single_digit(int(i / 100000)) + random.choice(lakh) + ",0,mag_pop4\t" + four_digit(i % 100000)
    else:
        s = single_digit(int(i / 100000)) + random.choice(lakh) + ",,mag_pop5\t" + five_digit(i % 100000)
    return s

print(six_digit(823546))


def seven_digit(i):
    if (i % 1000000 == 0):
        s = double_digit(int(i / 100000)) + random.choice(lakh) + ",00000,mag_pop0\t"
    elif (i % 1000000 < 10):
        s = double_digit(int(i / 100000)) + random.choice(lakh) + ",0000,mag_pop1\t" + single_digit(i % 1000000)
    elif (i % 1000000 < 100):
        s = double_digit(int(i / 100000)) + random.choice(lakh) + ",000,mag_pop2\t" + double_digit(i % 1000000)
    elif (i % 1000000 < 1000):
        s = double_digit(int(i / 100000)) + random.choice(lakh) + ",00,mag_pop3\t" + triple_digit(i % 1000000)
    elif (i % 1000000 < 10000):
        s = double_digit(int(i / 100000)) + random.choice(lakh) + ",0,mag_pop4\t" + four_digit(i % 1000000)
    elif (i % 1000000 < 100000):
        s = double_digit(int(i / 100000)) + random.choice(lakh) + ",,mag_pop5\t" + five_digit(i % 1000000)
    else:
        s= double_digit(int(i / 100000)) + random.choice(lakh) + ",,mag_pop5\t" + five_digit(i % 100000)
    return s


print(seven_digit(7646752))


def eight_digit(i):
    if (i % 10000000 == 0):
        s = single_digit(int(i / 10000000)) + "\t" + random.choice(crore) +",0000000,mag_pop0\t"
    elif (i % 10000000 < 10):
        s = single_digit(int(i / 10000000)) + "\t" + random.choice(crore) +",000000,mag_pop1\t" + single_digit(i % 10)
    elif (i % 10000000 < 100):
        s = single_digit(int(i / 10000000)) + "\t" + random.choice(crore) +",00000,mag_pop2\t" + double_digit(i % 100)
    elif (i % 10000000 < 1000):
        s = single_digit(int(i / 10000000)) + "\t" + random.choice(crore) +",0000,mag_pop3\t" + triple_digit(i % 1000)
    elif (i % 10000000 < 10000):
        s = single_digit(int(i / 10000000)) + "\t" + random.choice(crore) +",000,mag_pop4\t" + four_digit(i % 10000)
    elif (i % 10000000 < 100000):
        s = single_digit(int(i / 10000000)) + "\t" + random.choice(crore) +",00,mag_pop5\t" + five_digit(i % 100000)
    elif (i % 10000000 < 1000000):
        s = single_digit(int(i / 10000000)) + "\t" + random.choice(crore) +",0,mag_pop6\t" + six_digit(i % 1000000)
    else:
        s = single_digit(int(i / 10000000)) + "\t" + random.choice(crore) +",,mag_pop7\t\t" + seven_digit(i % 10000000)
    return s


print(eight_digit(59568752))

def encoding_numbers(i):
    s = ""
    if (i < 11):
        s = single_digit(i)
    elif (i < 100):
        s = double_digit(i)
    elif (i < 1000):
        s = triple_digit(i)
    elif (i < 10000):
        s = four_digit(i)
    elif (i < 100000):
        s = five_digit(i)
    elif (i < 1000000):
        s = six_digit(i)
    elif (i < 10000000):
        s = seven_digit(i)
    else:
        s = eight_digit(i)
    s = str(s) + " \n "

    print(s)
    return s


print(encoding_numbers(100))


count=1
for i in range(10000):
    j = 0
    if i <= 1250:
        j = random.choice(range(0, 21))
        s = str(count) + "-" + encoding_numbers(j) + "\n"
    elif 1250 < i <= 2500:
        j = random.choice(range(21, 100))
        s = str(count) + "-" + encoding_numbers(j) + "\n"
    elif 2500 < i <= 5000:
        j = random.choice(range(100, 1000))
        s = str(count) + "-" + encoding_numbers(j) + "\n"
    elif 5000 < i <= 7500:
        j = random.choice(range(1000, 10000))
        s = str(count) + "-" + encoding_numbers(j) + "\n"
    elif 7500 < i <= 8500:
        j = random.choice(range(10000, 100000))
        s = str(count) + "-" + encoding_numbers(j) + "\n"
    elif 8500 < i <= 9500:
        j = random.choice(range(100000, 1000000))
        s = str(count) + "-" + encoding_numbers(j) + "\n"
    elif 9500 < i <= 9750:
        j = random.choice(range(1000000, 10000000))
        s = str(count) + "-" + encoding_numbers(j) + "\n"
    else:
        j = random.choice(range(10000000, 100000000))
        s = str(count) + "-" + encoding_numbers(j) + "\n"
    count += 1
    f=open("English_in_Kannada_Numbers_Data.txt", "a", encoding="utf-8")
    f.write(s)
    f.close()
    print(s)


