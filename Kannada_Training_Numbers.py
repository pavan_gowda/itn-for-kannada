import random

# ones_digit = ["zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve",
#               "thirteen", "fourteen",
#               "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"]
# tens_digit = ["ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"]
# hundred = "hundred"
# thousand = "thousand"
# lakh = "lakhs"
# crore = "crores"
ones_digit =[[""],
    ["ಒಂದು"], ["ಎರಡು"], ["ಮೂರು"], ["ನಾಲ್ಕು"], ["ಐದು"], ["ಆರು"], ["ಏಳು"], ["ಎಂಟು"], ["ಒಂಬತ್ತು"], ["ಹತ್ತು"], ["ಹನ್ನೊಂದು"], ["ಹನ್ನೆರಡು"], ["ಹದಿಮೂರು"], ["ಹದಿನಾಲ್ಕು"], ["ಹದಿನೈದು"], ["ಹದಿನಾರು"], ["ಹದಿನೇಳು"],
    ["ಹದಿನೆಂಟು"], ["ಹತ್ತೊಂಬತ್ತು"], ["ಇಪ್ಪತ್ತು"], ["ಇಪ್ಪತ್ತೊಂದು"], ["ಇಪ್ಪತ್ತೆರಡು"], ["ಇಪ್ಪತ್ಮೂರು"], ["ಇಪ್ಪತ್ನಾಲ್ಕು", "ಇಪ್ಪತ್ನಾಕು"], ["ಇಪ್ಪತ್ತೈದು"], ["ಇಪ್ಪತ್ತಾರು"], ["ಇಪ್ಪತ್ತೇಳು"], ["ಇಪ್ಪತ್ತೆಂಟು"], ["ಇಪ್ಪತ್ತೊಂಬತ್ತು"], ["ಮೂವತ್ತು"], ["ಮೂವತ್ತೊಂದು"],
    ["ಮೂವತ್ತೆರಡು"], ["ಮೂವತ್ತಮೂರು"], ["ಮೂವತ್ತನಾಲ್ಕು"], ["ಮೂವತ್ತೈದು"], ["ಮೂವತ್ತಾರು"], ["ಮೂವತ್ತೇಳು"], ["ಮೂವತ್ತೆಂಟು"], ["ಮೂವತ್ತೊಂಬತ್ತು"], ["ನಲವತ್ತು"], ["ನಲವತ್ತೊಂದು"], ["ನಲವತ್ತೆರಡು"], ["ನಲವತ್ಮೂರು"], ["ನಲವತ್ತನಾಲ್ಕು"],
    ["ನಲವತ್ತೈದು"], ["ನಲವತ್ತಾರು"], ["ನಲವತ್ತೇಳು"], ["ನಲವತ್ತೆಂಟು"], ["ನಲವತ್ತೊಂಬತ್ತು"], ["ಐವತ್ತು"], ["ಐವತ್ತೊಂದು"], ["ಐವತ್ತೆರಡು"], ["ಐವತ್ತಮೂರು"], ["ಐವತ್ನಾಲ್ಕು"], ["ಐವತ್ತೈದು"], ["ಐವತ್ತಾರು"], ["ಐವತ್ತೇಳು"], ["ಐವತ್ತೆಂಟು"],
    ["ಐವತ್ತೊಂಬತ್ತು"], ["ಅರವತ್ತು"], ["ಅರವತ್ತೊಂದು"], ["ಅರವತ್ತೆರಡು"], ["ಅರವತ್ತಮೂರು"], ["ಅರವತ್ತನಾಲ್ಕು"], ["ಅರವತ್ತೈದು"], ["ಅರವತ್ತಾರು"], ["ಅರವತ್ತೇಳು"], ["ಅರವತ್ತೆಂಟು"], ["ಅರವತ್ತೊಂಬತ್ತು"], ["ಎಪ್ಪತ್ತು"], ["ಎಪ್ಪತ್ತೊಂದು"], ["ಎಪ್ಪತ್ತೆರಡು"],
    ["ಎಪ್ಪತ್ತಮೂರು"], ["ಎಪ್ಪತ್ತನಾಲ್ಕು"], ["ಎಪ್ಪತ್ತೈದು"], ["ಎಪ್ಪತ್ತಾರು"], ["ಎಪ್ಪತ್ತೇಳು"], ["ಎಪ್ಪತ್ತೆಂಟು"], ["ಎಪ್ಪತ್ತೊಂಬತ್ತು"], ["ಎಂಬತ್ತು"], ["ಎಂಭತ್ತೊಂದು"], ["ಎಂಭತ್ತೆರಡು"], ["ಎಂಭತ್ತಮೂರು"], ["ಎಂಭತ್ತನಾಲ್ಕು"], ["ಎಂಬತ್ತೈದು"], ["ಎಂಭತ್ತಾರು"],
    ["ಎಂಭತ್ತೇಳು"], ["ಎಂಭತ್ತೆಂಟು"], ["ಎಂಭತ್ತೊಂಬತ್ತು"], ["ತೊಂಬತ್ತು"], ["ತೊಂಬತ್ತೊಂದು"], ["ತೊಂಬತ್ತೆರಡು"], ["ತೊಂಬತ್ತಮೂರು"], ["ತೊಂಬತ್ತನಾಲ್ಕು"], ["ತೊಂಬತ್ತೈದು"], ["ತೊಂಬತ್ತಾರು"], ["ತೊಂಬತ್ತೇಳು"], ["ತೊಂಬತ್ತೆಂಟು"], ["ತೊಂಬತ್ತೊಂಬತ್ತು"]]

tens_digit = [[""], ["ಹತ್ತ್"], ["ಇಪ್ಪತ್ತ್"], ["ಮೂವತ್ತ್"], ["ನಲವತ್ತ್"], ["ಐವತ್ತ್"], ["ಅರವತ್ತ್"], ["ಎಪ್ಪತ್ತ್"], ["ಎಂಬತ್ತ್"], ["ತೊಂಬತ್ತ್"]]

hundred = [[""],["ನೂರು"],["ಇನ್ನೂರು"],["ಮುನ್ನೂರು"],["ನಾನೂರು"],["ಐನೂರು"],["ಆರ್ನೂರು"],["ಏಳುನೂರು"],["ಎಂಟುನೂರು"],["ಒಂಬೈನೂರು"] ]
hundred_double = [[""],["ನೂರ"], ["ಇನ್ನೂರ"],["ಮುನ್ನೂರ"],["ನಾನೂರ"], ["ಐನೂರ"],["ಆರ್ನೂರ"],["ಏಳ್ನೂರ"],["ಎಂಟ್ನೂರ"],["ಒಂಬೈನೂರ"]]
ones=[["ಒಂದು"],["ಎರಡು"],["ಮೂರು"],["ನಾಲ್ಕು"],["ಐದು"],["ಆರು"],["ಏಳು"],["ಎಂಟು"],["ಒಂಬತ್ತು"],["ಹತ್ತು"]]
thousand = ["ಸಾವಿರ"]
thousand_double = ["ಸಾವಿರದ"]
lakh = ["ಲಕ್ಷ"]
lakh_double = ["ಲಕ್ಷದ"]
crore = ["ಕೋಟಿ"]
crore_double = ["ಕೋಟಿಯ"]


def single_digit(i):
    print(str(i))
    s = random.choice(ones_digit[i]) + "," + str(i) + ",digit\t"
    return s

print(single_digit(20))


def double_digit(i):
    if (i < 20):
        s = single_digit(i)
    elif (i % 10 == 0):
        s = random.choice(tens_digit[int(i / 10)]) + "," + str(int(i)) + ",mag_pop0\t"
    elif (i < 100):
        s = random.choice(tens_digit[int(i / 10)]) + "," + str(int(i / 10)) + ",mag_pop1\t" + single_digit(i % 10)  # ones_digit[i%10-1] +","+str(i%10)+",digit"
    return s

print(double_digit(55))

def triple_digit(i):
    if (i % 100 == 0):
        s = random.choice(hundred[int(i / 100)]) + "," + str(int(i / 100)) + "00" +",mag_pop0\t"
    else:
        if((i % 100) < 10):
            s = random.choice(hundred_double[int(i / 100)]) + "," + str(int(i / 100)) + "0,mag_pop1\t" + "" + single_digit(i % 100)  # +""
        else:
            s = random.choice(hundred_double[int(i / 100)]) + "," + str(int(i / 100)) + ",mag_pop2\t" + "" + double_digit(i % 100)  # +""
    return s

print(triple_digit(545))


def four_digit(i):
    if (i % 1000 == 0):
        s = single_digit(int(i / 1000)) + random.choice(thousand) + ",000,mag_pop0\t"
    else:
        if (i % 1000 < 10):
            s = single_digit(int(i / 1000)) + random.choice(thousand_double) + ",00,mag_pop1\t" + single_digit(i % 1000)
        elif (i % 1000 < 100):
            s = single_digit(int(i / 1000)) + random.choice(thousand_double) + ",0,mag_pop2\t" + double_digit(i % 1000)
        else:
            s = single_digit(int(i / 1000)) + random.choice(thousand_double) + ",,mag_pop3\t" + triple_digit(i % 1000)
    return s


print(four_digit(6531))

def five_digit(i):
    if (i % 10000 == 0):
        s = double_digit(int(i / 1000)) + random.choice(thousand) + ",000,mag_pop0\t"
    elif (i % 10000 < 10):
        s = double_digit(int(i / 1000)) + random.choice(thousand_double) + ",00,mag_pop1\t" + single_digit(i % 10000)
    elif (i % 10000 < 100):
        s = double_digit(int(i / 1000)) + random.choice(thousand_double) + ",0,mag_pop2\t" + double_digit(i % 10000)
    elif (i % 10000 < 1000):
        s = double_digit(int(i / 1000)) + random.choice(thousand_double) + ",,mag_pop3\t" + triple_digit(i % 10000)
    else:
        s = double_digit(int(i / 1000)) + random.choice(thousand_double) + ",,mag_pop3\t" + triple_digit(i % 1000)
    return s

print(five_digit(88651))


def six_digit(i):
    if (i % 100000 == 0):
        s = single_digit(int(i / 100000)) + random.choice(lakh) + ",00000,mag_pop0\t"
    elif (i % 100000 < 10):
        s = single_digit(int(i / 100000)) + random.choice(lakh_double) + ",0000,mag_pop1\t" + single_digit(i % 100000)
    elif (i % 100000 < 100):
        s = single_digit(int(i / 100000)) + random.choice(lakh_double) + ",000,mag_pop2\t" + double_digit(i % 100000)
    elif (i % 100000 < 1000):
        s = single_digit(int(i / 100000)) + random.choice(lakh_double) + ",00,mag_pop3\t" + triple_digit(i % 100000)
    elif (i % 100000 < 10000):
        s = single_digit(int(i / 100000)) + random.choice(lakh_double) + ",0,mag_pop4\t" + four_digit(i % 100000)
    else:
        s = single_digit(int(i / 100000)) + random.choice(lakh_double) + ",,mag_pop5\t" + five_digit(i % 100000)
    return s

print(six_digit(664581))


def seven_digit(i):
    if (i % 1000000 == 0):
        s = double_digit(int(i / 100000)) + random.choice(lakh) + ",00000,mag_pop0\t"
    elif (i % 1000000 < 10):
        s = double_digit(int(i / 100000)) + random.choice(lakh_double) + ",0000,mag_pop1\t" + single_digit(i % 1000000)
    elif (i % 1000000 < 100):
        s = double_digit(int(i / 100000)) + random.choice(lakh_double) + ",000,mag_pop2\t" + double_digit(i % 1000000)
    elif (i % 1000000 < 1000):
        s = double_digit(int(i / 100000)) + random.choice(lakh_double) + ",00,mag_pop3\t" + triple_digit(i % 1000000)
    elif (i % 1000000 < 10000):
        s = double_digit(int(i / 100000)) + random.choice(lakh_double) + ",0,mag_pop4\t" + four_digit(i % 1000000)
    elif (i % 1000000 < 100000):
        s = double_digit(int(i / 100000)) + random.choice(lakh_double) + ",,mag_pop5\t" + five_digit(i % 1000000)
    else:
        s= double_digit(int(i / 100000)) + random.choice(lakh_double) + ",,mag_pop5\t" + five_digit(i % 100000)
    return s


print(seven_digit(7938465))


def eight_digit(i):
    if (i % 10000000 == 0):
        s = single_digit(int(i / 10000000)) + "\t" + random.choice(crore) +",0000000,mag_pop0\t"
    elif (i % 10000000 < 10):
        s = single_digit(int(i / 10000000)) + "\t" + random.choice(crore_double) +",000000,mag_pop1\t" + single_digit(i % 10)
    elif (i % 10000000 < 100):
        s = single_digit(int(i / 10000000)) + "\t" + random.choice(crore_double) +",00000,mag_pop2\t" + double_digit(i % 100)
    elif (i % 10000000 < 1000):
        s = single_digit(int(i / 10000000)) + "\t" + random.choice(crore_double) +",0000,mag_pop3\t" + triple_digit(i % 1000)
    elif (i % 10000000 < 10000):
        s = single_digit(int(i / 10000000)) + "\t" + random.choice(crore_double) +",000,mag_pop4\t" + four_digit(i % 10000)
    elif (i % 10000000 < 100000):
        s = single_digit(int(i / 10000000)) + "\t" + random.choice(crore_double) +",00,mag_pop5\t" + five_digit(i % 100000)
    elif (i % 10000000 < 1000000):
        s = single_digit(int(i / 10000000)) + "\t" + random.choice(crore_double) +",0,mag_pop6\t" + six_digit(i % 1000000)
    else:
        s = single_digit(int(i / 10000000)) + "\t" + random.choice(crore_double) +",,mag_pop7\t\t" + seven_digit(i % 10000000)
    return s


print(eight_digit(57858621))


# def triple_digit_hundred_and(i):
#     if (i % 100 == 0):
#         s = random.choice(hundred[int(i/ 100)]) + "," + str(int(i/ 100)*100)+ "," + "mag_pop0\t"
#     else:
#         if ((i % 100) < 10):
#             s = random.choice(hundred_double[int(i/ 100)]) + "," + str(int(i/ 100)*10) + ",mag_pop0\t" + "" + single_digit(i % 100)
#         else:
#             s = random.choice(hundred_double[int(i/ 100)]) + "," + str(int(i/ 100)) + ",mag_pop1\t" +""+ double_digit(i % 100)
#     return s+"\n"
#
#
# print(triple_digit_hundred_and(646))
#
#
# def four_digit_thousand_and(i):
#     if (i % 1000 == 0):
#         s = random.choice(single_digit(i)) + "," + str(int(i/ 1000)*1000) + ",mag_pop0\t"
#     else:
#         if (i % 1000 < 10):
#             s = random.choice(single_digit(i)) + "," + str(int(i/ 1000)) + ",mag_pop1\t" +""+ single_digit(i % 1000)
#         elif (i % 1000 < 100):
#             s = random.choice(single_digit(i)) + "," + str(int(i/ 1000)) + ",mag_pop2\t" +""+ double_digit(i % 1000)
#         else:
#             s = random.choice(single_digit(i)) + "," + str(int(i/ 1000)) + ",mag_pop3\t" + triple_digit(i % 1000)
#     return s+"\n"
#
#
# print(four_digit_thousand_and(8000))
#
#
# def hundred_and(i):
#     if (i % 1000 == 0):
#         s = random.choice(thousand) + "," + str(int(i/ 1000)*1000)+ "," + "mag_pop0\t"
#     else:
#         if ((i % 1000) < 10):
#             s = random.choice(ones_digit[int(i/ 1000)]) + "," + str(int(i/ 1000)) + ",digit\t" + thousand[1] + ",00,mag_pop0\t" + " " + single_digit(i % 1000)
#         elif ((i % 1000) < 100):
#             s = random.choice(ones_digit[int(i/ 1000)]) + "," + str(int(i/ 1000)) + ",digit\t" + thousand[1] + ",0,mag_pop1\t" + " " + double_digit(i % 1000)
#         else:
#             s = random.choice(ones_digit[int(i/ 1000)]) + "," + str(int(i/ 1000)) + ",digit\t" + thousand[1] + ",,mag_pop2\t" + " " + triple_digit_hundred_and(i% 1000)
#     return s+"\n"
#
#
# print(hundred_and(100))


#for i in range(1000,9999,8):
#    f.write(str(100)+"-"+ random.choice(thousand) + ",1,mag_pop3\t" +""+ triple_digit(i % 1000))
#    f.write(str(100) + "-" + random.choice(thousand) + ",1,mag_pop3\t" + "" + triple_digit_hundred_and(i % 1000))

def encoding_numbers(i):
    s = ""
    if (i < 11):
        s = single_digit(i)
    elif (i < 100):
        s = double_digit(i)
    elif (i < 1000):
        s = triple_digit(i)
    elif (i < 10000):
        s = four_digit(i)
    elif (i < 100000):
        s = five_digit(i)
    elif (i < 1000000):
        s = six_digit(i)
    elif (i < 10000000):
        s = seven_digit(i)
    else:
        s = eight_digit(i)
    s = str(s) + " \n "

    print(s)
    return s


print(encoding_numbers(100))


count=1
for i in range(10000):
    j = 0
    if i <= 1250:
        j = random.choice(range(0, 21))
        s = str(count) + "-" + encoding_numbers(j) + "\n"
    elif 1250 < i <= 2500:
        j = random.choice(range(21, 100))
        s = str(count) + "-" + encoding_numbers(j) + "\n"
    elif 2500 < i <= 5000:
        j = random.choice(range(100, 1000))
        s = str(count) + "-" + encoding_numbers(j) + "\n"
    elif 5000 < i <= 7500:
        j = random.choice(range(1000, 10000))
        s = str(count) + "-" + encoding_numbers(j) + "\n"
    elif 7500 < i <= 8500:
        j = random.choice(range(10000, 100000))
        s = str(count) + "-" + encoding_numbers(j) + "\n"
    elif 8500 < i <= 9500:
        j = random.choice(range(100000, 1000000))
        s = str(count) + "-" + encoding_numbers(j) + "\n"
    elif 9500 < i <= 9750:
        j = random.choice(range(1000000, 10000000))
        s = str(count) + "-" + encoding_numbers(j) + "\n"
    else:
        j = random.choice(range(10000000, 100000000))
        s = str(count) + "-" + encoding_numbers(j) + "\n"
    count += 1
    f=open("Kannada_Numbers_Data.txt", "a", encoding="utf-8")
    f.write(s)
    f.close()
    print(s)


